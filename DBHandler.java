import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by mark on 2016-11-06.
 */
public class DBHandler {
    private static String dbms = "mysql";
    private static String host = "localhost";
    private static int port = 3306;
    private static String user = "root";
    private static String pw = "Sw1mm3r1!";

    Connection conn = getConnection();

    // Query for customer search table
    private static final String CUSTOMER_WITH_DENTIST = "select c.cid, c.fname, c.lname, c.phone_Num, c.birthday, c.email, " +
                                          "c.address, d.fname, d.lname from Customer c, Attends a, Dentist d " +
                                          "where c.cid = a.cid and a.did = d.did and ";


    public static Connection getConnection() {
        Connection conn = null;
        while (conn == null) {
            try {
                Properties connectionProps = new Properties();
                connectionProps.put("user", user);
                connectionProps.put("pw", pw);
                conn = DriverManager.getConnection("jdbc: " +
                        dbms + "://" +
                        host + ":" +
                        port + "/" + connectionProps);
            } catch (SQLException e) {
                System.out.println("Connection loading...");
            }
        }
        return conn;
    }

    /* ------------------------------------------------------------------------------------------------------------------------------- //
    ----------------------------------------------- Login Methods -----------------------------------------------------------------
     */

    // Queries database for user's username and password
    // Returns 1 if user is customer
    //         2 if user is dentist
    //         3 if user is hygienist
    //         4 if user is receptionist
    //        -1 if user's information is not found
    // login type can only be one of: c, d, r, or h
    public int queryLoginInfo(String username, String pw) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.execute("select * from login_details where username = " + username);
        ResultSet rs = stmt.getResultSet();
        //result set has tuples
        if (rs.next()) {
            LoginInformation login = new LoginInformation(rs.getString(0), rs.getString(1), rs.getString(2), rs.getString(3));
            // Checks if hashpass = hash(pw + salt)
            if (!login.getHashPass()
                     .equals(BCrypt.hashpw(pw, login.getSalt()))) {
                return -1; // password does not match
            }

            switch(login.getType()) {
                case "c":
                    return 1;
                case "d":
                    return 2;
                case "h":
                    return 3;
                case "r":
                    return 4;
            }
        }
        return -1; // empty result set => username not found
    }


    /* ------------------------------------------------------------------------------------------------------------------------------- //
    ----------------------------------------------- Customer Methods -----------------------------------------------------------------
     */

    // The default view that an employee will have
    // Creates a list of customer objects that user must iterate through to handle
    public List<Customer> customerViewDefaultTable() throws SQLException {
        List<Customer> list = new ArrayList<>();
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.execute("select cid, fname, lname, phone_Num, birthday, email, address, d.fname, d.lname from Customer, Dentist d");
        ResultSet rs = stmt.getResultSet();
        while (rs.next()) {
            Customer c = new Customer(
                    rs.getInt(0),
                    rs.getString(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getDate(4),
                    rs.getString(5),
                    rs.getString(6));
            c.setDentist(rs.getString(7) + rs.getString(8));
            list.add(c);
        }
        return list;
    }

    // Filters customers by cid
    // Shows only those with cid = parameter cid
    // Creates a list of customer objects that user must iterate through to handle
    public List<Customer> customerSearchByCID(int cid) throws SQLException {
        List<Customer> list = new ArrayList<>();
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.execute(CUSTOMER_WITH_DENTIST + "cid = " + cid);
        ResultSet rs = stmt.getResultSet();
        while (rs.next()) {
            Customer c = new Customer(
                    rs.getInt(0),
                    rs.getString(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getDate(4),
                    rs.getString(5),
                    rs.getString(6));
            c.setDentist(rs.getString(7) + rs.getString(8));
            list.add(c);
        }
        return list;
    }

    // Filters customers by fname
    // Shows only those with fname = parameter fname
    // Creates a list of customer objects that user must iterate through to handle
    public List<Customer> customerSearchByName(int fname) throws SQLException {
        List<Customer> list = new ArrayList<>();
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.execute(CUSTOMER_WITH_DENTIST + "fname = " + fname);
        ResultSet rs = stmt.getResultSet();
        while (rs.next()) {
            Customer c = new Customer(
                    rs.getInt(0),
                    rs.getString(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getDate(4),
                    rs.getString(5),
                    rs.getString(6));
            c.setDentist(rs.getString(7) + rs.getString(8));
            list.add(c);
        }
        return list;
    }

    // Filters customers by lname
    // Shows only those with lname = parameter lname
    // Creates a list of customer objects that user must iterate through to handle
    public List<Customer> customerSearchByLastName(int lname) throws SQLException {
        List<Customer> list = new ArrayList<>();
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.execute(CUSTOMER_WITH_DENTIST + "lname = " + lname);
        ResultSet rs = stmt.getResultSet();
        while (rs.next()) {
            Customer c = new Customer(
                    rs.getInt(0),
                    rs.getString(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getDate(4),
                    rs.getString(5),
                    rs.getString(6));
            c.setDentist(rs.getString(7) + rs.getString(8));
            list.add(c);
        }
        return list;
    }

    public static void addCustomer(int cid, String fname, String lname, int phoneNum, Date dob, String email, String address) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO CUSTOMER VALUES (" + cid + " " + fname + " " + lname + " " + phoneNum + " " + dob + " " + email + " " + address + ")");
    }

    public static void updateCustomer(int cid, String fname, String lname, int phoneNum, Date dob, String email, String address) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("UPDATE customer SET fname = " + fname + ", lname = " + lname + ", phoneNum = " + phoneNum + ", dob = " + dob + ", email = " + email + ", address = " + address + " WHERE cid = " + cid);
    }

    public static void removeCustomer(int eid) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("DELETE FROM CUSTOMER WHERE eid = " + eid);
    }


    /* ------------------------------------------------------------------------------------------------------------------------------- //
   ----------------------------------------------- Employee Methods -----------------------------------------------------------------
    */

    public static void addEmployee(int eid, String fname, String lname, int salary, int age, String sex, String dob, int phoneNum) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO employee VALUES (" + eid + " " + salary + " " + age + " " + sex + " " + dob + " " + phoneNum + ")");
    }

    public static void updateEmployee(int eid, String fname, String lname, int salary, int age, String sex, String dob, int phoneNum) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("UPDATE employee SET salary = " + salary + ", age = " + age + ", sex = " + sex + ", dob = " + dob + ", phoneNum = " + phoneNum + " WHERE eid = " + eid);
    }

    public static void removeEmployee(int eid) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("DELETE FROM employee WHERE eid = " + eid);
    }

    public List<Employee> employeeSearchByName(int fname) throws SQLException {
        List<Employee> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        statement.execute("SELECT * FROM employee WHERE fname LIKE '%" + fname + "%');
                ResultSet rs = statement.getResultSet();
        while (rs.next()) {
            Employee e = new Employee(
                    rs.getInt(0),
                    rs.getString(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getInt(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getInt(7));
            list.add(e);
        }
        return list;
    }

    // Search employee
    public List<Employee> employeeSearchByLastName(int lname) throws SQLException {
        List<Employee> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        statement.execute("SELECT * FROM employee WHERE lname LIKE '%" + lname + "%');
                ResultSet rs = stmt.getResultSet();
        while (rs.next()) {
            Employee e = new Employee(
                    rs.getInt(0),
                    rs.getString(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getInt(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getInt(7));
            list.add(e);
        }
        return list;
    }


      /* ------------------------------------------------------------------------------------------------------------------------------- //
    ----------------------------------------------- Appointment Methods -----------------------------------------------------------------
     */

    public static void addAppointment(int aid, Date start, Date end, int cid) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("INSERT INTO appointment VALUES (" + aid " " + start + " " + end + " " + cid + ")");
    }

    public static void updateAppointment(int aid, Date start, Date end, int cid) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("UPDATE appointment SET start = " + start + ", end = " + end + " WHERE aid = " + aid);
    }

    public static void removeAppointment(int aid) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("DELETE FROM appointment WHERE aid = " + aid);
    }

    // Finds specific appointment records for a customer
    public static List<Appointment> getUpcomingCustomerAppointments(int cid) throws SQLException {
        String query = "select a.num, a.type, a.fromTime, a.toTime from Customer c, Appointment a where " +
                "c.cid = " + cid + " and a.cid = " + cid + " and CURRENT_TIMESTAMP <= a.fromTime";
        List<Appointment> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        statement.execute(query);
        ResultSet rs = statement.getResultSet();
        while (rs.next()) {
            Appointment a = new Appointment(
                    rs.getInt(0),
                    rs.getString(1),
                    rs.getTimestamp(2),
                    rs.getTimestamp(3));
            list.add(a);
        }
        return list;
    }

    // Finds past appointment records for a customer
    public static List<Appointment> getPastCustomerAppointments(int cid) throws SQLException {
        String query = "select a.num, a.type, a.fromTime, a.toTime from Customer c, Appointment a where " +
                "c.cid = " + cid + " and a.cid = " + cid + " and CURRENT_TIMESTAMP > a.fromTime";
        List<Appointment> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        statement.execute(query);
        ResultSet rs = statement.getResultSet();
        while (rs.next()) {
            Appointment a = new Appointment(
                    rs.getInt(0),
                    rs.getString(1),
                    rs.getTimestamp(2),
                    rs.getTimestamp(3));
            list.add(a);
        }
        return list;
    }

    /* ------------------------------------------------------------------------------------------------------------------------------- //
    ----------------------------------------------- Bill Methods -----------------------------------------------------------------
     */

    public static void addBill(int bid, int cid, int amount, String type, int did) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("INSERT INTO bill VALUES (" + bid " " + cid + " " + amount + " " + type + " " + did + ")");
    }

    public static void updateBill(int cid, int amount, String type, int did) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("UPDATE bill SET amount = " + amount + ", type = " + type + ", did = " + did + " WHERE cid = " + cid);
    }

    public static void removeBill(int bid) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("DELETE FROM bill WHERE bid = " + bid);
    }

    // Finds  bills for customer
    public static List<Bill> getCustomerBills(int cid) throws SQLException {
        String query = "SELECT * from Customer c, Bill b where " +
                "c.cid = " + cid + " and b.cid = " + cid;
        List<Bill> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        statement.execute(query);
        ResultSet rs = statement.getResultSet();
        while (rs.next()) {
            Bill b = new Bill(
                    rs.getInt(0),
                    rs.getInt(1),
                    rs.getInt(2),
                    rs.getString(3),
                    rs.getInt(4));
            list.add(b);
        }
        return list;
    }

    /* ------------------------------------------------------------------------------------------------------------------------------- //
    ----------------------------------------------- Medicine Methods -----------------------------------------------------------------
     */

    public static void addMedicine(int code, double cost, String description) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("INSERT INTO medicine VALUES (" + code " " + cost + " " + description + ")");
    }

    public static void updateMedicine(int code, double cost, String description) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("UPDATE medicine SET cost = " + cost + ", description = " + description + " WHERE code = " + code);
    }

    public static void removeMedicine(int code) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("DELETE FROM medicine WHERE code = " + code);
    }

    public static void addTreats(int code, int cid) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("INSERT INTO treats VALUES (" + code " " + cid + ")");
    }

    public static void removeTreats(int code) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("DELETE FROM medicine WHERE code = " + code + " AND cid = " + cid);
    }

    // Finds  medicines for customer
    public static List<Medicine> getCustomerMedicines(int cid) throws SQLException {
        String query = "SELECT m.code, m.cost, m.description from Treats t, Medicine m where " +
                "t.cid = " + cid;
        List<Medicine> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        statement.execute(query);
        ResultSet rs = statement.getResultSet();
        while (rs.next()) {
            Medicine m = new Medicine(
                    rs.getInt(0),
                    rs.getDouble(1),
                    rs.getString(2));
            list.add(m);
        }
        return list;
    }
}
